<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	

	<title>Siram</title>

	<link rel="shortcut icon" href="<?php echo site_url('assets/images/logo.png') ?>">

	<!-- Bootstrap -->
	<link href="<?php echo site_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo site_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo site_url('assets/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">

	 <link href="<?php echo site_url('assets/vendors/select2/dist/css/select2.min.css') ?>" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo site_url('assets/css/custom.min.css') ?>" rel="stylesheet">

		<!-- jQuery -->
		<script src="<?php echo site_url('assets/vendors/jquery/dist/jquery.min.js') ?>"></script>

		<!-- Bootstrap -->
	<script src="<?php echo site_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
	<!-- FastClick -->
	<script src="<?php echo site_url('assets/vendors/fastclick/lib/fastclick.js') ?>"></script>
	<!-- NProgress -->
	<script src="<?php echo site_url('assets/vendors/nprogress/nprogress.js') ?>"></script>
	<!-- validator -->


	<script src="<?php echo site_url('assets/vendors/select2/dist/js/select2.full.min.js') ?>"></script>
    <!-- Parsley -->
    <script src="<?php echo site_url('assets/vendors/parsleyjs/dist/parsley.min.js') ?>"></script>

    <script src="<?php echo site_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/jszip/dist/jszip.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/vendors/pdfmake/build/vfs_fonts.js') ?>"></script>
	
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><img src="<?php echo site_url('assets/images/logo-kominfo.png') ?>" height="50px;" alt=""> <span>Siram</span></a>
					</div>

					<div class="clearfix"></div>
					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<?php if ($this->session->userdata('status')=="login") {
								
								?>
									<li <?php echo isset($is_active) ? $is_active=='dashboard' ? 'class="active"' : NULL : NULL ?>><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-home"></i> Dashboard</a>
									</li>
									<li <?php echo isset($is_active) ? $is_active=='master/tower' || $is_active=='master/provider' || $is_active=='master/kecamatan' ? 'class="active"' : NULL : NULL ?> ><a><i class="fa fa-edit"></i> Master <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php echo isset($is_active) ? $is_active=='master/tower' || $is_active=='master/provider' || $is_active=='master/kecamatan' ? 'style="display : block"' : NULL : NULL ?> >
											<li <?php echo isset($is_active) ? $is_active=='master/tower' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('master/tower') ?>">Tower</a></li>
											<li <?php echo isset($is_active) ? $is_active=='master/provider' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('master/provider') ?>">Provider</a></li>
											<li <?php echo isset($is_active) ? $is_active=='master/kecamatan' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('master/kecamatan') ?>">Kecamatan</a></li>
										</ul>
									</li>
									<li <?php echo isset($is_active) ? $is_active=='pembayaran/penerbitan_skrd' || $is_active=='pembayaran/konfirmasi_pembayaran'? 'class="active"' : NULL : NULL ?> ><a><i class="fa fa-money"></i> Pembayaran<span
												class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php echo isset($is_active) ? $is_active=='pembayaran/penerbitan_skrd' || $is_active=='pembayaran/konfirmasi_pembayaran'? 'style="display : block"' : NULL : NULL ?>>
											<li <?php echo isset($is_active) ? $is_active=='pembayaran/penerbitan_skrd' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('pembayaran/penerbitan_skrd') ?>">Penerbitan SKRD</a></li>
											<li <?php echo isset($is_active) ? $is_active=='pembayaran/konfirmasi_pembayaran' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('pembayaran/konfirmasi_pembayaran') ?>">Konfirmasi Pembayaran</a></li>
										</ul>
									</li>
									<!-- <li><a><i class="fa fa-print"></i> Laporan<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li><a href="<?php echo site_url('laporan/cetak_skrd') ?>">Cetak SKRD</a></li>
										</ul>
									</li> -->
								<?php
								} else {
								?>
									<li <?php echo isset($is_active) ? $is_active=='dashboard' ? 'class="active"' : NULL : NULL ?>><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-home"></i> Dashboard</a>
									</li>
									<li <?php echo isset($is_active) ? $is_active=='provider/tower' || $is_active=='provider/skrd' || $is_active=='provider/konfirm' ? 'class="active"' : NULL : NULL ?> ><a><i class="fa fa-edit"></i> Provider <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php echo isset($is_active) ? $is_active=='provider/tower' || $is_active=='provider/skrd' || $is_active=='provider/konfirm' ? 'style="display : block"' : NULL : NULL ?> >
											<li <?php echo isset($is_active) ? $is_active=='provider/tower' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('provider/tower') ?>">Data Tower</a></li>
											<li <?php echo isset($is_active) ? $is_active=='provider/skrd' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('provider/skrd') ?>">Data Tagihan</a></li>
											<li <?php echo isset($is_active) ? $is_active=='provider/konfirm' ? 'class="current-page"' : NULL : NULL ?>><a href="<?php echo site_url('provider/konfirm') ?>">Konfirmasi Pembayaran</a></li>
										</ul>
									</li>
									
								<?php
								}

								?>								
							</ul>
						</div>

					</div>
					<!-- /sidebar menu -->

				</div>
			</div>


			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
									<img src="<?php echo site_url('assets/images/user.png') ?>" alt=""><?php print_r($this->session->userdata("nama")) ?>
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<!-- <li><a href="javascript:;"><span>Settings</span></a></li> -->
									<li><a href="<?php echo site_url('dashboard/logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
								</ul>
							</li>
							<li role="presentation" class="dropdown">
								<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-envelope-o"></i>
									<span class="badge bg-green"><?php echo isset($notif['jml_notif']) ? $notif['jml_notif'] : '0' ?></span>
								</a>
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">

								<?php
                                    if ($this->session->userdata('provider_login')== TRUE) {
                                        ?>
								
								<?php
                                                                
                                    if (isset($notif['no_tagihan'])) :
                                        if (sizeof($notif['no_tagihan'])>0) {
                                            foreach ($notif['no_tagihan'] as $row):
                                ?>
									<li>
										<a href="<?php echo site_url('provider/skrd/view/'.$row['id']) ?>">
											<span>
											<span>#<?php echo $row['id'] ?></span>
											<span class="time">3 mins ago</span>
											</span>
											<span class="message">
											Anda mempunyai tagihan yang harus dibayar dengan no tagihan #<?php echo $row['id'] ?>
											</span>
										</a>
									</li>
								<?php
                                            endforeach;
                                        } else {
                                            ?>
									<li>
										<a>
											<span>
											<span></span>
											<span class="time"></span>
											</span>
											<span class="message">
											Anda tidak memiliki notifikasi...
											</span>
										</a>
									</li>
								<?php
                                        }
                                        endif;
                                    }
								?>

									<li>
									<div class="text-center">
										<a>
										<strong>See All Alerts</strong>
										<i class="fa fa-angle-right"></i>
										</a>
									</div>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->
