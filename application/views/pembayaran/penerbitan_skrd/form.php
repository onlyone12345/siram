<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Penerbitan SKRD</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form class="form-horizontal form-label-left" action="<?php echo site_url($form_action) ?>" method="POST" data-parsley-validate>
							
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jatuh_tempo">Tanggal Jatuh Tempo
									<span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<input type="date" id="jatuh_tempo" name="jatuh_tempo" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['jatuh_tempo']) ? $edited['jatuh_tempo'] : NULL ?>">
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan
									<span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<textarea id="keterangan" name="keterangan"
											class="form-control col-md-7 col-xs-12"><?php echo isset($edited['keterangan']) ? $edited['keterangan'] : NULL ?></textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<a href="javascript:history.back()" class="btn btn-primary">Cancel</a>
									<button id="send" type="submit" class="btn btn-success">Terbitkan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script>
$(document).ready(function () {

	$('.select2').select2().on('change', function () {
		var npwrd = $('[name="npwrd"]').val();
		$.ajax({
			cache: false,
			type: "POST",
			url : "<?php echo site_url('master/tower/get_single') ?>",
			data : {npwrd:npwrd},
			success: function(response) {
				$('#alamat').html(response.alamat);
				$('#provider').val(response.provider);
				var rpmt2 = parseInt(response.rpmt);
				$('#rpmt').val(rpmt2.format(0, 3, '.', ','));
			}
		});
	});

	<?php if(isset($edited['npwrd'])) : ?>

	var newOption = new Option(<?php echo "'".$edited['npwrd']."'" ?>, <?php echo "'".$edited['npwrd']."'" ?>, false, false);
	$('[name="npwrd"]').append(newOption).trigger('change');

	<?php endif; ?>

	
});
</script>