<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Konfirmasi Pembayaran</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form class="form-horizontal form-label-left" action="<?php echo site_url($form_action) ?>" method="POST" enctype="multipart/form-data" data-parsley-validate>
							<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_skrd">No Tagihan
									<span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
										<select class="form-control input-sm select2" 
											data-ajax--url="<?php echo site_url('provider/konfirm/get_select2_no_tagihan') ?>"
											data-ajax--type="POST"
											data-ajax--dataType="json" 
											name="id_skrd"
											required="required"
										></select>
												
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nominal">Jumlah Tagihan
									<span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<input id="jumlah_tagihan" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['jumlah_tagihan']) ? $edited['jumlah_tagihan'] : NULL ?>">
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nominal">Jumlah Nominal Pembayaran
									<span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<input type="number" id="nominal" name="nominal" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['jatuh_tempo']) ? $edited['jatuh_tempo'] : NULL ?>">
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan
									
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<textarea id="keterangan" name="keterangan"
											class="form-control col-md-7 col-xs-12" rows="5"><?php echo isset($edited['keterangan']) ? $edited['keterangan'] : NULL ?></textarea>
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jatuh_tempo">Foto Bukti
									<span class="required">*</span>
								</label>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<input type="file" id="foto_bukti" name="foto_bukti" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['jatuh_tempo']) ? $edited['jatuh_tempo'] : NULL ?>">
								</div>
							</div>

							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<a href="javascript:history.back()" class="btn btn-primary">Cancel</a>
									<button id="send" type="submit" class="btn btn-success">Sumbit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script>
$(document).ready(function () {
	$('.select2').select2();

	$('[name="id_skrd"]').on('change', function name(params) {
		var id_skrd = $('[name="id_skrd"]').val();
		$.ajax({
			cache: false,
			type: "POST",
			url : "<?php echo site_url('provider/konfirm/get_single') ?>",
			data : {id_skrd:id_skrd},
			success: function(response) {
				var harus_dibayar = parseInt(response.results.harus_dibayar);
				$('#jumlah_tagihan').val(harus_dibayar.format(0, 3, '.', ','));
			}
		});
	});

	<?php if(isset($edited['npwrd'])) : ?>

	var newOption = new Option(<?php echo "'".$edited['npwrd']."'" ?>, <?php echo "'".$edited['npwrd']."'" ?>, false, false);
	$('[name="npwrd"]').append(newOption).trigger('change');

	<?php endif; ?>

	
});
</script>