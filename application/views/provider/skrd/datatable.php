<!-- page content -->
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><?php echo ucwords($title) ?></h2>
						<!-- <ul class="nav navbar-right panel_toolbox">
							<a href="<?php echo site_url($add_url)?>" class="btn btn-success"><i
									class="fa fa-plus"></i> Tambah</a>
						</ul> -->
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<table id="datatable_custom" class="table table-striped table-bordered">
							<thead>
								<?php echo $datatable_header ?>
							</thead>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal_delete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
		<form action="<?php echo site_url($delete_url) ?>" method="POST">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id">
				<div class="alert alert-danger" role="alert">
                    <strong>Peringatan!</strong> Apakah anda ingin menghapus data ini ?
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-danger">Delete</button>
			</div>
		</form>
		</div>
	</div>
</div>
<!-- page content-->
<script>
	$(document).ready(function () {
		$('.modal_delete').on('show.bs.modal', function (event) {
            var id = $(event.relatedTarget).data('id');
            $(this).find('.modal-body').find('input[name="id"]').val(id);
        });
		
		$('#datatable_custom').dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "<?php echo site_url($datatable_url) ?>",
				"type": "POST"
			},
			"columns": [<?php echo implode('',$datatable_column) ?>]
				});
			});

</script>
