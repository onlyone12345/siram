<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Login || Siram </title>

	<!-- Bootstrap -->
	<link href="<?php echo site_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo site_url('assets/vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo site_url('assets/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
	<!-- Animate.css -->
	<link href="<?php echo site_url('assets/vendors/animate.css/animate.min.css') ?>" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?php echo site_url('assets/css/custom.css') ?>" rel="stylesheet">
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form action="<?php echo site_url('login/login_action') ?>" method="POST">
						<div class="login-logo">
                            <img src="<?php echo site_url('assets/images/logo-kominfo.png') ?>" alt="logo-kominfo">
                        </div>
                        <h1>Siram</h1>
						<div>
							<input type="text" class="form-control" placeholder="Username" name="username"
								required="" />
						</div>
						<div>
							<input type="password" class="form-control" placeholder="Password" name="password"
								required="" />
						</div>
						<div>
							<button type="submit" class="btn btn-primary submit">Log in</button>
							<!-- <a class="reset_pass" href="#">Lupa Password ?</a> -->
						</div>
						<br>
						<br>
						<?php 
							echo $this->session->flashdata("flash_data")?  '<p style="color:red">Username Atau Password Salah </p>' : NULL;
						?>
						<div class="clearfix"></div>

						<div class="clearfix"></div>
						<br />

					</form>
				</section>
			</div>

		</div>
	</div>
</body>

</html>
