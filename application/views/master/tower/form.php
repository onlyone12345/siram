<style>
	.form-horizontal .control-label {
		text-align: left !important;
	}
	#rpmt {
		height : 110px;
		background-color : #ddd;
		text-align : right;
	}

	#rpmt p {
		line-height : 110px;
		font-size : 300%;
		margin-right : 10px;
	}
</style>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah Data Tower</h2>

						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form id="form" class="form-horizontal form-label-left"
							action="<?php echo site_url($form_action) ?>" method="POST" data-parsley-validate>
							<div class="row">
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="npwrd">NPWRD
											<span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-12 col-xs-12">
										
											<input type="text" class="form-control" name="npwrd" required="required"
													value="<?php echo isset($edited['npwrd']) ? $edited['npwrd'] : NULL ?>" readonly>
												
											
										</div>
									</div>
									<!-- data-validate-length-range="6" -->
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="alamat">Alamat Menara
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<textarea id="alamat" required="required" name="alamat"
												class="form-control col-md-7 col-xs-12"><?php echo isset($edited['alamat']) ? $edited['alamat'] : NULL ?></textarea>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="kecamatan">Kecamatan
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
												<select class="form-control input-sm select2" 
													data-ajax--url="<?php echo site_url('master/kecamatan/get_select2') ?>"
													data-ajax--type="POST"
													data-ajax--dataType="json" 
													name="id_kecamatan"
													required="required"
												></select>
														
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="id_provider">provider
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<select class="select2_ajax form-control" tabindex="-1" name="id_provider" id="id_provider" required="required">
												
											</select>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="tipe">Tipe
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<!-- <input id="tipe" name="tipe" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['tipe']) ? $edited['tipe'] : NULL ?>"> -->
												<select class="select2 form-control" tabindex="-1" name="tipe">
													<?php echo isset($edited['tipe']) ? '<option selected>'.$edited['tipe'].'</option>' : NULL ?>
													<option>Tunggal</option>
													<option>Bersama</option>
													<option>Kamuflase</option>
											</select>
										</div>
									</div>

									
									<!-- <div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="lat">Latitude
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="lat" name="lat" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['lat']) ? $edited['lat'] : NULL ?>">
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="lon">Longitude
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="lon" name="lon" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['lon']) ? $edited['lon'] : NULL ?>">
										</div>
									</div> -->
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12">

									<!-- <div class="item form-group">

										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="tinggi">Tinggi
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="tinggi" name="tinggi" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['tinggi']) ? $edited['tinggi'] : NULL ?>">
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="luas">Luas
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="luas" name="luas" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['luas']) ? $edited['luas'] : NULL ?>">
										</div>
									</div> -->
									
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="jenis">Koef. Jenis
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="jenis" name="jenis" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['jenis']) ? $edited['jenis'] : NULL ?>">
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="jarak">Koef. Jarak
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="jarak" name="jarak" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['jarak']) ? $edited['jarak'] : NULL ?>">
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="biaya">Biaya
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="biaya" name="biaya" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['biaya']) ? $edited['biaya'] : 2527100 ?>">
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="rpmt">RPMT
											<span class="required">*</span>
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<input id="rpmt" type="hidden" name="rpmt" required="required"
												class="form-control col-md-7 col-xs-12"
												value="<?php echo isset($edited['rpmt']) ? $edited['rpmt'] : NULL ?>"
												readonly>

											<div id="rpmt">
												<p><?php echo isset($edited['rpmt']) ? number_format($edited['rpmt']) : NULL ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<a href="javascript:history.back()" class="btn btn-primary">Cancel</a>
									<button id="send" type="submit" class="btn btn-success">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script>
	
	$(document).ready(function () {
		var menara = [
			{
				"jenis_menara" : "Tunggal",
				"koefisien" : 1.1	
			},
			{
				"jenis_menara" : "Bersama",
				"koefisien" : 1.0	
			},
			{
				"jenis_menara" : "Kamuflase",
				"koefisien": 0.9	
			}
		];

		console.log(menara[0].koefisien);

		$('.select2').select2();


		$('#generate').click(function () {
			$('[name="npwrd"]').val('');
		});
		$('form#form').click(function (e) {
			// e.preventDefault();

		});
		

		$("[name='jenis'], [name='jarak'], [name='biaya'], [name='tipe'], [name='id_kecamatan']").on('change', function () {
			var tipe = $('[name="tipe"]').val();

			var filter = menara.find(function(menara, i){
				if(menara.jenis_menara == tipe){
					return i+1;
				}
			});

			console.log(filter.koefisien);
			$('[name="jenis"]').val(filter.koefisien);
						
			var jenis = parseFloat($('[name="jenis"]').val());
			var jarak = parseFloat($('[name="jarak"]').val());
			var biaya = parseFloat($('[name="biaya"]').val());
			var rpmt  = ((jenis + jarak) / 2) * biaya;

			$('[name="rpmt"]').val(rpmt);
			$('#rpmt > p').html(rpmt.format(0, 3, '.', ','));

			// console.log();
		});
		

		$('.select2_ajax').select2({
			ajax: {
				url: '<?php echo site_url('master/provider/get_select2') ?>',
				dataType: 'json',
				method : 'POST'
				// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			}
		});
		<?php if(isset($edited['provider']) && isset($edited['id_provider'])) : ?>

		var newOption = new Option(<?php echo "'".$edited['provider']."'" ?>, <?php echo "'".$edited['id_provider']."'" ?>, false, false);
		$('.select2_ajax').append(newOption).trigger('change');

		<?php endif; ?>

		<?php if(isset($edited['kecamatan']) && isset($edited['id_kecamatan'])) : ?>

		var newOption = new Option(<?php echo "'".$edited['kecamatan']."'" ?>, <?php echo "'".$edited['id_kecamatan']."'" ?>, false, false);
		$('[name="id_kecamatan"]').append(newOption).trigger('change');

		<?php endif; ?>

		$('[name="id_provider"], [name="id_kecamatan"]').on('change', function name(params) {
			var kecamatan = $('[name="id_kecamatan"]').val();
			var id_provider = $('[name="id_provider"]').val();
			var npwrd_before = '<?php echo isset($edited['npwrd']) ? $edited['npwrd'] : NULL ?>';
			$.ajax({
				cache: false,
				type: "POST",
				url : "<?php echo site_url('master/tower/get_npwrd') ?>",
				data : {kecamatan:kecamatan,id_provider:id_provider, npwrd_before:npwrd_before},
				success: function(response) {
					$('[name="npwrd"]').val(response);

				}
			});
		});

		$('[name="id_kecamatan"]').on('change', function name(params) {
			var kecamatan = $('[name="id_kecamatan"]').val();
			$.ajax({
				cache: false,
				type: "POST",
				url : "<?php echo site_url('master/kecamatan/get_single') ?>",
				data : {kecamatan:kecamatan},
				success: function(response) {
					$('[name="jarak"]').val(response.results[0].koefisien);
				}
			});
		});
		
		$('[name="kecamatan"]').trigger('change');

		// $('form#form').parsley().on('field:validated', function() {
		// 	var ok = $('.parsley-error').length === 0;
		// 	$('.bs-callout-info').toggleClass('hidden', !ok);
		// 	$('.bs-callout-warning').toggleClass('hidden', ok);
		// })
		// .on('form:submit', function() {
		// 	return false; // Don't submit form for this demo
		// });
	});

</script>
