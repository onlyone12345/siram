<style>
	.form-horizontal .control-label {
		text-align: left !important;
	}
	#rpmt {
		height : 110px;
		background-color : #ddd;
		text-align : right;
	}

	#rpmt p {
		line-height : 110px;
		font-size : 300%;
		margin-right : 10px;
	}
</style>
<!-- page content -->
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Data Tower</h2>

						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div id="form" class="form-horizontal form-label-left">
							<div class="row">
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="npwrd">NPWRD
										
										</label>
										<div class="col-md-6 col-sm-12 col-xs-12">
											<h4>: <?php echo isset($npwrd) ? $npwrd : NULL ?></h4>
										</div>
									</div>
									<!-- data-validate-length-range="6" -->
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="alamat">Alamat
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($alamat) ? $alamat : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="kecamatan">Kecamatan
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($kecamatan) ? $kecamatan : NULL ?></h4>
										
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="provider">Provider
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($provider) ? $provider : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12"
											for="alamat_provider">Alamat Provider
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($alamat_provider) ? $alamat_provider : NULL ?></h4>
										</div>
									</div>

									<!-- <div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="lat">Latitude
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($lat) ? $lat : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-4 col-sm-8 col-xs-12" for="lon">Longitude
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($lon) ? $lon : NULL ?></h4>
										</div>
									</div> -->
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12">

									<!-- <div class="item form-group">

										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="tinggi">Tinggi
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($tinggi) ? $tinggi : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="luas">Luas
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($luas) ? $luas : NULL ?></h4>
										</div>
									</div> -->
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="tipe">Tipe
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($tipe) ? $tipe : NULL ?></h4>
										</div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="jenis">Koef. Jenis
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($jenis) ? $jenis : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="jarak">Koef. Jarak
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($jarak) ? $jarak : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="biaya">Biaya
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<h4>: <?php echo isset($biaya) ? number_format($biaya) : NULL ?></h4>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-2 col-sm-8 col-xs-12" for="rpmt">RPMT
											
										</label>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div id="rpmt">
												<p><?php echo isset($rpmt) ? number_format($rpmt) : NULL ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
	
