<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah Data Tower</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form class="form-horizontal form-label-left" action="<?php echo site_url($form_action) ?>" method="POST" data-parsley-validate>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Perusahaan
									<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="nama" name="nama" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['nama']) ? $edited['nama'] : NULL ?>">
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="alias">Alias
									<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="alias" name="alias" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['alias']) ? $edited['alias'] : NULL ?>">
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Email
									<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="email" name="email" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['email']) ? $edited['email'] : NULL ?>">
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Telp
									<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="telp" name="telp" required="required"
										class="form-control col-md-7 col-xs-12" value="<?php echo isset($edited['telp']) ? $edited['telp'] : NULL ?>">
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat
									Perusahaan
									<span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea id="alamat" required="required" name="alamat"
										class="form-control col-md-7 col-xs-12"><?php echo isset($edited['alamat']) ? $edited['alamat'] : NULL ?></textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<a href="javascript:history.back()" class="btn btn-primary">Cancel</a>
									<button id="send" type="submit" class="btn btn-success">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
