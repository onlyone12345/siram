<!-- page content -->
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><?php echo ucwords($title) ?></h2>
						<ul class="nav navbar-right panel_toolbox">
							<div class="btn-group" id="export">
								
							</div>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" style="position:relative">
						<!-- <div class="collapse" id="filterCollapse"> -->
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label>Kecamatan</label>
										<select class="form-control input-sm select2" 
											data-ajax--url="<?php echo site_url('master/kecamatan/get_select2') ?>"
											data-ajax--type="POST"
											data-ajax--dataType="json" 
											name="filter[id_kecamatan]"
											required="required"
										></select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Provider</label>
										<select class="form-control input-sm select2" 
											data-ajax--url="<?php echo site_url('master/provider/get_select2') ?>"
											data-ajax--type="POST"
											data-ajax--dataType="json" 
											name="filter[id_provider]" 
										></select>
									</div>
								</div>
								
							<!-- </div> -->
						</div>
						<!-- <button 
							style="position:absolute; bottom:190px; left:50%; transform:translateX(-50%); z-index:99;" 
							type="button" class="btn btn-default" data-toggle="collapse" data-target="#filterCollapse" aria-expanded="false">
							Filter
						</button> -->
						
						<table id="datatable_custom" class="table table-striped table-bordered">
							<thead>
								<?php echo $datatable_header ?>
							</thead>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- page content-->
<script>
	$(document).ready(function () {
		$('.modal_delete').on('show.bs.modal', function (event) {
            var id = $(event.relatedTarget).data('id');
            $(this).find('.modal-body').find('input[name="npwrd"]').val(id);
        });
		
		var iTable = $('#datatable_custom').DataTable({
			buttons :[
			{
				extend: 'print',
				className: 'btn btn-sm btn-success',
				text:'<i class="fa fa-print"></i> Print',
				footer: true,
				action : function( e, dt, button, config ) {
					window.open('<?php echo site_url('laporan/cetak_skrd') ?>/cetak?' + $.param(dt.ajax.params()),'_blank');
				}
			}],
			processing: true,
			serverSide: true,
			ajax: {
				url  : "<?php echo site_url($datatable_url) ?>",
				type : "POST",
				error: function(){
					$('.dataTable').find('tbody').html('<tr><td colspan="<?php echo sizeof($datatable_column);?>" style="text-align:center;">No data available in table</td></tr>');
					$(".dataTables_processing").css("display","none");
				},
				data : function ( d ) {
					var obj = {};
					$('[name^=filter]').each(function(){
						var key = $(this).attr('name');
						var val = $(this).val();
						obj[key] = val;
					});
					return $.extend( {}, d, obj);
					}
				},
				columns: [<?php echo implode('',$datatable_column) ?>],
				initComplete : function () {
					iTable.buttons().container().appendTo( "#export" );
					console.log('selesai');
				},
				drawCallback : function(settings){
					$(".select2").select2({ width:"100%",allowClear:true,placeholder:"Filter"});
					$('[name^=filter]').on("dp.change change keyup", function(e){
						e.preventDefault();
						iTable.draw();
					});
				},
				});
		
			});
</script>
