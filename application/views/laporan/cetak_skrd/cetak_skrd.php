<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Skrd</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
	<script src="main.js"></script>
	<style>
		* {
			padding: 0;
			margin: 0;
			box-sizing: border-box;
		}

		@page {
			size: auto;
			margin: 0mm;
			/* padding-top : 50px; */
		}

		@media print {
			.for-looping {page-break-after: always;}
		}

		.for-looping {
			margin: 60px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		tr,
		th,
		td {
			/* border : 1px solid black; */

		}

		table.table-1 tr.section-1 {
			text-align: center;

		}

		table.table-1 tr.section-1 th:first-child {
			width: 10%;
			padding: 10px 20px;
			border-left: 1px solid black;
			border-top: 1px solid black;

		}

		table.table-1 tr.section-1 td:nth-child(2) {
			padding: 5px;
			font-size: 80%;
			border-top: 1px solid black;
		}

		table.table-1 tr.section-1 th:last-child {
			width: 10%;
			padding: 10px 20px;
			border-top: 1px solid black;
			border-right: 1px solid black;
		}

		table.table-2 tr td {
			padding: 5px 10PX;
			font-size: 80%;
		}

		table.table-2 tr.section-1 td:nth-child(1) {
			width: 20%;
			border-top: 1px solid black;
			border-left: 1px solid black;
		}

		table.table-2 tr.section-1 td:nth-child(2) {
			width: 2%;
			border-top: 1px solid black;
		}

		table.table-2 tr.section-1 td:nth-child(3) {
			width: 30%;
			border-top: 1px solid black;
			border-right: 1px solid black;
		}

		table.table-2 tr.section-1 td:nth-child(4) {
			width: 10%;
			border-top: 1px solid black;
		}

		table.table-2 tr.section-1 td:nth-child(5) {
			width: 2%;
			border-top: 1px solid black;
		}

		table.table-2 tr.section-1 td:nth-child(6) {
			width: 20%;
			border-top: 1px solid black;
			border-right: 1px solid black;
		}



		table.table-2 tr.section-2 td:nth-child(1) {
			width: 25%;
			border-left: 1px solid black;
		}

		table.table-2 tr.section-2 td:nth-child(2) {
			width: 2%;
		}

		table.table-2 tr.section-2 td:nth-child(3) {
			width: 30%;
			border-right: 1px solid black;
		}

		table.table-2 tr.section-2 td:nth-child(4) {
			width: 10%;
		}

		table.table-2 tr.section-2 td:nth-child(5) {
			width: 2%;
		}

		table.table-2 tr.section-2 td:nth-child(6) {
			width: 20%;
			border-right: 1px solid black;
		}


		table.table-3 {
			border-top: 1px solid black;
			border-left: 1px solid black;
			border-right: 1px solid black;
		}

		table.table-3 tr td {
			padding: 2px 10PX;
			font-size: 80%;
		}

		table.table-3 tr.section-2 td:nth-child(1) {
			width: 25%;
		}

		table.table-3 tr.section-2 td:nth-child(2) {
			width: 2%;
		}

		table.table-4 {
			text-align: center;
			border-top: 1px solid black;
			border-left: 1px solid black;
			border-right: 1px solid black;
			font-size: 90%;
		}

		table.table-4 tr td {
			padding: 2px 5px;
			background-color: #42e5f4;
		}

		table.table-4 tr td:nth-child(1) {
			padding: 2px 5px;
			border-right: 1px solid black;
			width: 70%;
		}


		table.table-5 {
			font-size: 80%;
			border-top: 1px solid black;
			border-left: 1px solid black;
			border-right: 1px solid black;
		}

		table.table-5 tr td {
			padding: 2px 5px;
		}

		table.table-5 tr td:nth-child(1) {
			width: 25%;
		}



		table.table-6 {
			border-top: 1px solid black;
			border-left: 1px solid black;
			border-right: 1px solid black;
		}

		table.table-6 tr td {
			padding: 2px 5px;
			font-size: 80%;
			/* border : 1px solid black; */
		}

		table.table-6 tr td:nth-child(1) {
			width: 25%;
			padding-left: 10%;
		}

		table.table-6 tr td:nth-child(2) {
			width: 2%;
		}

		table.table-6 tr td:nth-child(3) {
			width: 14%;
		}

		table.table-6 tr td:nth-child(4) {
			width: 2%;
		}

		table.table-6 tr td:nth-child(5) {
			width: 2%;
		}

		table.table-6 tr td:nth-child(6) {
			width: 23%;
			border-right: 1px solid black;
		}

		table.table-6 tr td:nth-child(7) {
			width: 2%;
		}

		table.table-6 tr.section-1 {
			border-top: 1px solid black;
		}


		table.table-7 {
			border-top: 1px solid black;
			border-left: 1px solid black;
			border-right: 1px solid black;
		}

		table.table-7 tr td {
			padding: 5px;
			font-size: 80%;
			/* border : 1px solid black; */
		}

		table.table-7 tr td:nth-child(1) {
			width: 15%;
			text-align: right;
		}


		table.table-8 {
			border: 1px solid black;
		}

		table.table-8 tr td {
			padding: 2px 5px;
			font-size: 80%;
		}

		.table-9,
		.table-10 {
			font-size: 80%;
		}

		.table-9 {
			margin-bottom: 8px;
		}



		table.table-9 tr td:nth-child(1) {
			padding: 2px 5px;
			width: 52%;
		}

		table.table-10 tr td:nth-child(1) {
			padding: 2px 5px;
			width: 50%;
		}

		table.table-10 tr td.for-uu {
			padding: 5% 15% 5% 0;
		}

		table.table-10 tr td.for-uu div {
			padding: 15px;
		}

	</style>
</head>

<body>
	<?php
		foreach ($data as $row) :
	
	?>
	<div class="for-looping">
		<table class="table-1">
			<tr class="section-1">
				<th><img src="<?php echo site_url('assets/images/logo-lamsel.png') ?>" alt="" height="75px"> </th>
				<td>
					<h2>PEMERINTAH KABUPATEN LAMPUNG SELATAN</h2>
					<h2>DINAS KOMUNIKASI DAN INFORMATIKA</h2>
					<p>Jalan Mustafa Kemal, Telp. (0727) 321178, Fax. (0727) 321178, KALIANDA, 35513</p>
				</td>
				<th><img src="<?php echo site_url('assets/images/logo-kominfo.png') ?>" alt="" height="75px"> </th>
			</tr>
			<tr class="section-1">
				<th colspan="3">
					<h3>SURAT KETETAPAN RETRIBUSI DAERAH (SKRD)</h3>
					<h3>RETRIBUSI PENGENDALIAN MENARA TELEKOMUNIKASI </h3>
				</th>
			</tr>
		</table>
		<table class="table-2">
			<tr class="section-1">
				<td>MASA RETRIBUSI</td>
				<td>:</td>
				<td>JAN – DES TAHUN 2019</td>
				<td>NPWRD</td>
				<td>:</td>
				<td><?php echo $row['npwrd'] ?></td>
			</tr>
			<tr class="section-2">
				<td>TANGGAL JATUH TEMPO</td>
				<td>:</td>
				<td>31 DESEMBER TAHUN 2019</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<table class="table-3">
			<tr class="section-1">
				<td colspan="3"><b>WAJIB RETRIBUSI</b></td>
			</tr>
			<tr class="section-2">
				<td>Nama Perusahaan</td>
				<td>:</td>
				<td><?php echo $row['provider'] ?></td>
			</tr>
			<tr class="section-2">
				<td>Alamat Perusahaan</td>
				<td>:</td>
				<td><?php echo $row['alamat_provider'] ?></td>
			</tr>
		</table>
		<table class="table-3">
			<tr class="section-1">
				<td colspan="3"><b>OBJEK RETRIBUSI</b></td>
			</tr>
			<tr class="section-2">
				<td>Lokasi Menara</td>
				<td>:</td>
				<td><?php echo $row['alamat'] ?> </td>
			</tr>
			<tr class="section-2">
				<td>Desa / Kelurahan</td>
				<td>:</td>
				<td></td>
			</tr>
			<tr class="section-2">
				<td>Kecamatan</td>
				<td>:</td>
				<td><?php echo $row['kecamatan'] ?></td>
			</tr>
			<tr class="section-2">
				<td>Latitude</td>
				<td>:</td>
				<td><?php echo $row['lat'] ?></td>
			</tr>
			<tr class="section-2">
				<td>Longitude</td>
				<td>:</td>
				<td><?php echo $row['lon'] ?></td>
			</tr>
		</table>
		<table class="table-4">
			<tr class="section-1">
				<td><b>URAIAN RETRIBUSI</b></td>
				<td><b>JUMLAH</b></td>
			</tr>
		</table>
		<table class="table-5">
			<tr>
				<td><b>KODE REKENING</b></td>
				<td><b>:</b></td>
			</tr>
		</table>
		<table class="table-6">
			<tr>
				<td>Koef. Jenis Menara</td>
				<td>:</td>
				<td><?php echo $row['jenis'] ?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Koef. Jarak Tempuh</td>
				<td>:</td>
				<td><?php echo $row['jarak'] ?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Biaya Pengawasan</td>
				<td>:</td>
				<td>Rp <?php echo number_format($row['biaya']) ?>,-</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>RPMT</td>
				<td>=</td>
				<td style="text-align : center; border-bottom : 1px solid black"> <?php echo $row['jenis'] ?> + <?php echo $row['jarak'] ?> </td>
				<td rowspan="2"> x </td>
				<td rowspan="2"> Rp </td>
				<td rowspan="2"> <?php echo number_format($row['biaya']) ?>,- </td>
				<td rowspan="2"> Rp </td>
				<td rowspan="2"> <?php echo number_format($row['rpmt']) ?>,- </td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td style="text-align : center"> 2 </td>
			</tr>
			<tr class="section-1">
				<td></td>
				<td></td>
				<td>Sanksi</td>
				<td>:</td>
				<td>a.</td>
				<td>Bunga % </td>
				<td>Rp</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>b.</td>
				<td>Kenaikan </td>
				<td>Rp</td>
				<td></td>
			</tr>
			<tr class="section-1">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td> <b>JUMLAH KESELURUHAN</b> </td>
				<td>Rp</td>
				<td><?php echo number_format($row['rpmt']) ?>,-</td>
			</tr>
		</table>
		<table class="table-7">
			<tr>
				<td>Terbilang : </td>
				<td><b>Dua Juta Tujuh Ratus Tujuh Puluh Sembilan Ribu Delapan Ratus Sepuluh Rupiah </b></td>
			</tr>
		</table>
		<table class="table-8">
			<tr>
				<td colspan="2">
					<b> PERHATIAN : </b>
				</td>
			</tr>
			<tr>
				<td style="vertical-align : top;">1.</td>
				<td>
					Harap penyetoran dilakukan ke Kas Daerah Kabupaten Lampung Selatan (Dinas Kominfo Kabupaten Lampung
					Selatan melalui: <br>
					- PT BANK LAMPUNG, Cabang Kalianda, Lampung Selatan <br>
					- Nomor Rekening : <b> 383.00.09.00003.9 </b> <br>
					- Ayat : 4.1.2.02.01 <br>
				</td>
			</tr>
			<tr>
				<td style="vertical-align : top;">2.</td>
				<td>
					Apabila Surat Ketetapan Retribusi ini tidak atau kurang dibayar lewat waktu paling lama15 (lima
					belas) hari setelah surat ketetapan diterima atau (tanggal jatuh tempo) dikenakanSanksi berupa bunga
					2 % per bulan dari jumlah Retribusi yang ditagih.
				</td>
			</tr>
			<tr>
				<td style="vertical-align : top;">3.</td>
				<td>
					Agar mencantumkan <b> NPWRD </b> objek retribusi dalam tanda bukti pelunasan/pembayara
				</td>
			</tr>
		</table>
		<br>
		<br>
		<table class="table-9">
			<tr>
				<td></td>
				<td> Kalianda , 14 Februari 2019</td>
			</tr>
		</table>
		<table class="table-10">
			<tr>
				<td class="for-uu">
					<div style="border : 1px solid black;">
						<i>
							<b>Dasar Penagihan :</b> <br>
							Peraturan Daerah Kabupaten Lampung Selatan Nomor 10 Tahun 2012 tentang Retribusi Pengendalian
							Menara Telekomunikasi
						</i>
					</div>
				</td>
				<td style="text-align:center;">Plt. KEPALA DINAS KOMUNIKASI DAN INFORMATIKA <br> KABUPATEN LAMPUNG
					SELATAN <br><br><br><br><br><br> <b><u>M. SEFRI MASDIAN, S.Sos</u></b> <br> Pembina Tingkat I <br>
					NIP. 19710909 200003 1 007 </td>
			</tr>
		</table>
        <div class="for-break">
        </div>
	</div>
	<?php
		endforeach;
	?>
</body>
<script>
	setTimeout(() => {
		window.print();
	}, 100);

	setTimeout(() => {
		window.close();
	}, 100);
</script>
</html>


