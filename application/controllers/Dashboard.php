<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct(){
		parent::__construct();
		no_access();
	}

    public function index()
    {
        $header = array(
			"is_active" => "dashboard"
		);

        $this->load->view('part/header', $header);
        $this->load->view('default/dashboard');
        $this->load->view('part/footer');
    }

    public function sendmail()
    {

        $to = 'dronedrone12345@gmail.com';
        $subject = 'Test Kirim';
        $messages = 'Coba Saya Kirimkan Email Ke Anda';
            
        $headers = 'From: <onlyfirdaus@gmail.com>' . "SKRD"; //bagian ini diganti sesuai dengan email dari pengirim
        
        mail($to, $subject, $messages, $headers);
        if(1==1)
        {
            echo "pengiriman berhasil";
        }
        else 
        {
            echo "pengiriman gagal";
        }
    }

    function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

}
?>