<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi_pembayaran extends CI_Controller {
	function __construct(){
		parent::__construct();
		no_access();
	}

	public function index()
	{
		$header = array(
			"is_active" => "pembayaran/konfirmasi_pembayaran"
		);

		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
		$data = $this->konfirmasi_pembayaran_m->config_datatable();
		$this->load->view('part/header', $header);
		$this->load->view('pembayaran/konfirmasi_pembayaran/datatable', $data);
		$this->load->view('part/footer');
	}

	public function add()
	{
		$header = array(
			"is_active" => "pembayaran/konfirmasi_pembayaran"
		);

		$data =array(
			"form_action" => "pembayaran/konfirmasi_pembayaran/insert"
		);

		$this->load->view('part/header', $header);
		$this->load->view('pembayaran/konfirmasi_pembayaran/form', $data);
		$this->load->view('part/footer');
	}

	public function insert()
	{
		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
		if($this->pembayaran_m->insert($_POST)) {
			redirect(site_url('pembayaran/konfirmasi_pembayaran'));
		}
	}

	public function datatable()
    {
        $this->load->model('pembayaran/konfirmasi_pembayaran_m');
        $outp = $this->konfirmasi_pembayaran_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}
	public function konfirm( $id = NULL )
	{
		$header = array(
			"is_active" => "pembayaran/konfirmasi_pembayaran"
		);

		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
		$data = array(
			"form_action" => 'pembayaran/konfirmasi_pembayaran/konfirm_action',
			"edited" => $this->konfirmasi_pembayaran_m->edit($id)
		);

		$this->load->view('part/header', $header);
		$this->load->view('pembayaran/konfirmasi_pembayaran/form', $data);
		$this->load->view('part/footer');
	}

	public function update( $id = NULL )
	{
		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
		if ($this->konfirmasi_pembayaran_m->update($id ,$_POST)) {
			redirect(site_url('pembayaran/konfirmasi_pembayaran'));
		}
	}

	public function delete()
	{
		$id = $_POST['id'];
		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
		if ($this->konfirmasi_pembayaran_m->delete($id)) {
			redirect(site_url('pembayaran/konfirmasi_pembayaran'));
		}
	}

	public function get_select2()
	{
		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
        $outp['results'] = $this->konfirmasi_pembayaran_m->get_select2();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function get_select2_tower()
	{
		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
        $outp['results'] = $this->konfirmasi_pembayaran_m->get_select2_tower();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function konfirm_action()
	{
		$id = $_POST['id'];
		$this->load->model('pembayaran/konfirmasi_pembayaran_m');
		if ($this->konfirmasi_pembayaran_m->konfirm_action($id)) {
			redirect(site_url('pembayaran/konfirmasi_pembayaran'));
		}
	}

	
}
