<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerbitan_skrd extends CI_Controller {
	function __construct(){
		parent::__construct();
		no_access();
	}

	public function index()
	{
		$header = array(
			"is_active" => "pembayaran/penerbitan_skrd"
		);

		$this->load->model('pembayaran/penerbitan_skrd_m');
		$data = $this->penerbitan_skrd_m->config_datatable();
		$this->load->view('part/header', $header);
		$this->load->view('pembayaran/penerbitan_skrd/datatable', $data);
		$this->load->view('part/footer');
	}

	public function add()
	{
		$header = array(
			"is_active" => "pembayaran/penerbitan_skrd"
		);

		$data =array(
			"form_action" => "pembayaran/penerbitan_skrd/insert"
		);

		$this->load->view('part/header', $header);
		$this->load->view('pembayaran/penerbitan_skrd/form', $data);
		$this->load->view('part/footer');
	}

	public function insert()
	{
		$this->load->model('pembayaran/penerbitan_skrd_m');
		if($this->penerbitan_skrd_m->insert($_POST)) {
			redirect(site_url('pembayaran/penerbitan_skrd'));
		}
	}

	public function datatable()
    {
        $this->load->model('pembayaran/penerbitan_skrd_m');
        $outp = $this->penerbitan_skrd_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}

	public function view( $id = NULL )
	{
		$header = array(
			"is_active" => "pembayaran/penerbitan_skrd"
		);

		$this->load->model('pembayaran/penerbitan_skrd_m');
		$data = $this->penerbitan_skrd_m->config_datatable_tower($id);
		$this->load->view('part/header', $header);
		$this->load->view('pembayaran/penerbitan_skrd/datatable_tower', $data);
		$this->load->view('part/footer');
	}

	public function datatable_tower($id = NULL)
    {
	

        $this->load->model('pembayaran/penerbitan_skrd_m');
        $outp = $this->penerbitan_skrd_m->datatable_tower($id, $_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function edit( $id = NULL )
	{
		$this->load->model('pembayaran/pembayaran_m');
		$data = array(
			"form_action" => 'pembayaran/pembayaran/update/'.$id,
			"edited" => $this->pembayaran_m->edit($id)
		);
        // $data['edited']= $this->pembayaran_m->edit($id);
		// print_r($data);
		$this->load->view('part/header');
		$this->load->view('pembayaran/pembayaran/form', $data);
		$this->load->view('part/footer');
	}

	public function update( $id = NULL )
	{
		$this->load->model('pembayaran/pembayaran_m');
		if ($this->pembayaran_m->update($id ,$_POST)) {
			redirect(site_url('pembayaran/pembayaran'));
		}
	}

	public function delete()
	{
		$id = $_POST['id'];
		$this->load->model('pembayaran/penerbitan_skrd_m');
		if ($this->penerbitan_skrd_m->delete($id)) {
			redirect(site_url('pembayaran/penerbitan_skrd'));
		}
	}

	public function get_select2()
	{
		$this->load->model('pembayaran/pembayaran_m');
        $outp['results'] = $this->pembayaran_m->get_select2();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function get_select2_tower()
	{
		$this->load->model('pembayaran/pembayaran_m');
        $outp['results'] = $this->pembayaran_m->get_select2_tower();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	
	public function print_skrd( $id = NULL )
	{
		$this->load->model('pembayaran/penerbitan_skrd_m');
        $outp = $this->penerbitan_skrd_m->print_skrd($id);
		$this->load->view('laporan/cetak_skrd/cetak_skrd', $outp);
		
	}


	
	public function print_single( $id = NULL )
	{
		$this->load->model('pembayaran/penerbitan_skrd_m');
        $outp = $this->penerbitan_skrd_m->print_single($id);
		$this->load->view('laporan/cetak_skrd/cetak_skrd', $outp);
		
	}





	
}
