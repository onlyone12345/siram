<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirm extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        $this->load->model('login/provider_login_m');
        if (isset($_GET['v']) || $this->session->userdata('unik')) {
            $kode_unik = isset($_GET['v']) ? $_GET['v'] : $this->session->userdata('unik');

            $get = $this->provider_login_m->get_unik($kode_unik);

            $cek = $get->num_rows();
            if ($cek > 0) {
                $provider = $get->result();
                $id_provider = $provider[0]->id;
                $nama_provider = $provider[0]->nama;
                $data_session = array(
                    'unik' => $kode_unik,
                    'id_provider' => $id_provider,
                    'status' => "provider_login",
                    'nama' => $nama_provider,
                    );
     
                $this->session->set_userdata($data_session);
            } else {
                redirect('login');
            }
        } else {
            redirect('login');
        }
    }
        
    public function index()
	{
        $this->load->model('part/header_m');
        
        $header = array(
            "notif" => $this->header_m->get_notif(),
        );

        $data = array(
            "form_action" => "provider/konfirm/action"
        );
        
		$this->load->view('part/header', $header);
		$this->load->view('provider/konfirm/form', $data);		
		$this->load->view('part/footer');
    }

    public function action()
    {   
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        // $config['file_name']            = 'bukti_';
        $config['max_size']             = 0;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( !$this->upload->do_upload('foto_bukti'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        }
        else
        {
            // $data = array('upload_data' => $this->upload->data());

            // print_r($data);

            $_POST['foto_bukti'] = $this->upload->data('file_name');
            $_POST['id_provider'] = $this->session->userdata('id_provider');
            
            $this->load->model('provider/konfirm_m');
            $insert = $this->konfirm_m->insert($_POST);

            if($insert){
                redirect(site_url('provider'));
            }           
        }
    }

    public function get_select2_no_tagihan()
	{
		$this->load->model('provider/konfirm_m');
        $outp['results'] = $this->konfirm_m->get_select2_no_tagihan();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }
    
    public function get_single()
	{
		$this->load->model('provider/konfirm_m');
        $outp['results'] = $this->konfirm_m->get_single($_POST['id_skrd']);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}
    
}