<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skrd extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        $this->load->model('login/provider_login_m');
        if (isset($_GET['v']) || $this->session->userdata('unik')) {
            $kode_unik = isset($_GET['v']) ? $_GET['v'] : $this->session->userdata('unik');

            $get = $this->provider_login_m->get_unik($kode_unik);
            $cek = $get->num_rows();
            if ($cek > 0) {
                $provider = $get->result();
                $id_provider = $provider[0]->id;
                $nama_provider = $provider[0]->nama;
                $data_session = array(
                    'unik' => $kode_unik,
                    'id_provider' => $id_provider,
                    'status' => "provider_login",
                    'nama' => $nama_provider,
                    );
     
                $this->session->set_userdata($data_session);
            } else {
                redirect('login');
            }
        } else {
            redirect('login');
        }
    }
        
    public function index()
	{
        $this->load->model('part/header_m');
        
        $header = array(
            "notif" => $this->header_m->get_notif(),
        );

		$this->load->model('provider/skrd_provider_m');
		$data = $this->skrd_provider_m->config_datatable();
		$this->load->view('part/header', $header);
		$this->load->view('provider/skrd/datatable', $data);
		$this->load->view('part/footer');
    }
    
    public function datatable()
    {
        $this->load->model('provider/skrd_provider_m');
        $outp = $this->skrd_provider_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

    }
    
    public function view( $id = NULL )
	{
        $this->load->model('part/header_m');
        
        $header = array(
            "notif" => $this->header_m->get_notif(),
        );

		$this->load->model('provider/skrd_provider_m');
		$data = $this->skrd_provider_m->config_datatable_tower($id);
		$this->load->view('part/header', $header);
		$this->load->view('provider/skrd/datatable_tower', $data);
		$this->load->view('part/footer');
	}

	public function datatable_tower($id = NULL)
    {
        $this->load->model('provider/skrd_provider_m');
        $outp = $this->skrd_provider_m->datatable_tower($id, $_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }
    
    public function print_skrd( $id = NULL )
	{
		$this->load->model('provider/skrd_provider_m');
        $outp = $this->skrd_provider_m->print_skrd($id);
		$this->load->view('laporan/cetak_skrd/cetak_skrd', $outp);
		
	}


	
	public function print_single( $id = NULL )
	{
		$this->load->model('provider/skrd_provider_m');
        $outp = $this->skrd_provider_m->print_single($id);
		$this->load->view('laporan/cetak_skrd/cetak_skrd', $outp);
		
	}


}





