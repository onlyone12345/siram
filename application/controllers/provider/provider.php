<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider extends CI_Controller
{
    function __construct(){
        parent::__construct();
        
        $this->load->model('login/provider_login_m');
        if(isset($_GET['v']) || $this->session->userdata('unik')){
            $kode_unik = isset($_GET['v']) ? $_GET['v'] : $this->session->userdata('unik');

            $get = $this->provider_login_m->get_unik($kode_unik);
            $cek = $get->num_rows();
            if($cek > 0){
                $provider = $get->result();
                $id_provider = $provider[0]->id;
                $nama_provider = $provider[0]->nama;
                $data_session = array(
                    'unik' => $kode_unik,
                    'id_provider' => $id_provider,
                    'status' => "provider_login",
                    'nama' => $nama_provider,
                    );
     
                $this->session->set_userdata($data_session);
                
            }else{
                redirect('login');
            }

        }else{
            redirect('login');
        }

    //    print_r($this->session->userdata());

        // if($this->provider_login_m->get_unik($_GET['v'])){
            
        // }else {
        //     redirect('login');
        // }
	
	}

    public function index()
    {
        redirect('provider/skrd');
    }

    public function destroy()
    {
       $this->session->sess_destroy();
    }

    public function get_cek()
    {
        
    }


}