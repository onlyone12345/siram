<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tower extends CI_Controller {
	function __construct(){
		parent::__construct();
		no_access();
	}


	public function index()
	{
		$header = array(
			"is_active" => "master/tower"
		);

		$this->load->model('master/tower_m');
		$data = $this->tower_m->config_datatable();
		$this->load->view('part/header', $header);
		$this->load->view('master/tower/datatable', $data);
		$this->load->view('part/footer');
	}

	public function add()
	{
		$header = array(
			"is_active" => "master/tower"
		);

		$data =array(
			"form_action" => "master/tower/insert"
		);

		$this->load->view('part/header', $header);
		$this->load->view('master/tower/form', $data);
		$this->load->view('part/footer');
	}

	public function insert()
	{
		$this->load->model('master/tower_m');
		if($this->tower_m->insert($_POST)) {
			redirect(site_url('master/tower'));
		}
	}

	public function datatable()
    {
        $this->load->model('master/tower_m');
        $outp = $this->tower_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}
	public function edit( $id = NULL )
	{
		$header = array(
			"is_active" => "master/tower"
		);

		$this->load->model('master/tower_m');
		$data = array(
			"form_action" => 'master/tower/update/'.$id,
			"edited" => $this->tower_m->edit($id)
		);
        // $data['edited']= $this->tower_m->edit($id);
		// print_r($data);
		$this->load->view('part/header', $header);
		$this->load->view('master/tower/form', $data);
		$this->load->view('part/footer');
	}

	public function update( $id = NULL )
	{
		$this->load->model('master/tower_m');
		if ($this->tower_m->update($id ,$_POST)) {
			redirect(site_url('master/tower'));
		}
	}

	public function delete()
	{
		$npwrd = $_POST['npwrd'];
		$this->load->model('master/tower_m');
		if ($this->tower_m->delete($npwrd)) {
			redirect(site_url('master/tower'));
		}
	}

	public function view( $id = NULL )
	{
		$header = array(
			"is_active" => "master/tower"
		);

		$this->load->model('master/tower_m');
		$data = $this->tower_m->view($id);
        // $data['edited']= $this->tower_m->edit($id);
		// print_r($data);
		$this->load->view('part/header', $header);
		$this->load->view('master/tower/view', $data);
		$this->load->view('part/footer');
	}

	public function get_select2()
	{
		$this->load->model('master/tower_m');
        $outp['results'] = $this->tower_m->get_select2();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function get_npwrd()
	{
		$this->load->model('master/tower_m');
        $outp = $this->tower_m->get_npwrd($_POST['kecamatan'], $_POST['id_provider'], $_POST['npwrd_before']);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}

	public function get_single()
	{
		$this->load->model('master/tower_m');
        $outp = $this->tower_m->get_single($_POST['npwrd']);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}
}
