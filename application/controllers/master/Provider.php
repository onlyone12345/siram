<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider extends CI_Controller {

	function __construct(){
		parent::__construct();
		no_access();
	}

	public function index()
	{
		$header = array(
			"is_active" => "master/provider"
		);

		$this->load->model('master/provider_m');
		$data = $this->provider_m->config_datatable();
		$this->load->view('part/header', $header);
		$this->load->view('master/provider/datatable', $data);
		$this->load->view('part/footer');
	}

	public function add()
	{
		$header = array(
			"is_active" => "master/provider"
		);

		$data =array(
			"form_action" => "master/provider/insert"
		);

		$this->load->view('part/header');
		$this->load->view('master/provider/form', $data);
		$this->load->view('part/footer');
	}

	public function insert()
	{
		$this->load->model('master/provider_m');
		if($this->provider_m->insert($_POST)) {
			redirect(site_url('master/provider'));
		}
	}

	public function datatable()
    {
        $this->load->model('master/provider_m');
        $outp = $this->provider_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}
	public function edit( $id = NULL )
	{
		$header = array(
			"is_active" => "master/provider"
		);

		$this->load->model('master/provider_m');
		$data = array(
			"form_action" => 'master/provider/update/'.$id,
			"edited" => $this->provider_m->edit($id)
		);
        // $data['edited']= $this->provider_m->edit($id);
		// print_r($data);
		$this->load->view('part/header', $header);
		$this->load->view('master/provider/form', $data);
		$this->load->view('part/footer');
	}

	public function update( $id = NULL )
	{
		$this->load->model('master/provider_m');
		if ($this->provider_m->update($id ,$_POST)) {
			redirect(site_url('master/provider'));
		}
	}

	public function delete()
	{
		$id = $_POST['id'];
		$this->load->model('master/provider_m');
		if ($this->provider_m->delete($id)) {
			redirect(site_url('master/provider'));
		}
	}

	public function get_select2()
	{
		$this->load->model('master/provider_m');
        $outp['results'] = $this->provider_m->get_select2();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function get_alamat()
	{
		$this->load->model('master/provider_m');
        $outp['results'] = $this->provider_m->get_alamat(2);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}
}
