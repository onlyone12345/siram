<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {
	function __construct(){
		parent::__construct();
		no_access();
	}
	
	public function index()
	{
		$header = array(
			"is_active" => "master/kecamatan"
		);

		$this->load->model('master/kecamatan_m');
		$data = $this->kecamatan_m->config_datatable();
		$this->load->view('part/header', $header);
		$this->load->view('master/kecamatan/datatable', $data);
		$this->load->view('part/footer');
	}

	public function add()
	{
		$header = array(
			"is_active" => "master/kecamatan"
		);	

		$data =array(
			"form_action" => "master/kecamatan/insert",
		);

		$this->load->view('part/header');
		$this->load->view('master/kecamatan/form', $data);
		$this->load->view('part/footer');
	}

	public function insert()
	{
		$this->load->model('master/kecamatan_m');
		if($this->kecamatan_m->insert($_POST)) {
			redirect(site_url('master/kecamatan'));
		}
	}

	public function datatable()
    {
        $this->load->model('master/kecamatan_m');
        $outp = $this->kecamatan_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}
	public function edit( $id = NULL )
	{
		$header = array(
			"is_active" => "master/kecamatan"
		);

		$this->load->model('master/kecamatan_m');
		$data = array(
			"form_action" => 'master/kecamatan/update/'.$id,
			"edited" => $this->kecamatan_m->edit($id)
		);
        // $data['edited']= $this->kecamatan_m->edit($id);
		// print_r($data);
		$this->load->view('part/header', $header);
		$this->load->view('master/kecamatan/form', $data);
		$this->load->view('part/footer');
	}

	public function update( $id = NULL )
	{
		$this->load->model('master/kecamatan_m');
		if ($this->kecamatan_m->update($id ,$_POST)) {
			redirect(site_url('master/kecamatan'));
		}
	}

	public function delete()
	{
		$id = $_POST['id'];
		$this->load->model('master/kecamatan_m');
		if ($this->kecamatan_m->delete($id)) {
			redirect(site_url('master/kecamatan'));
		}
	}

	public function get_select2()
	{
		$this->load->model('master/kecamatan_m');
        $outp['results'] = $this->kecamatan_m->get_select2();
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function get_alamat()
	{
		$this->load->model('master/kecamatan_m');
        $outp['results'] = $this->kecamatan_m->get_alamat(2);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

	public function get_single()
	{
		$this->load->model('master/kecamatan_m');
        $outp['results'] = $this->kecamatan_m->get_single($_POST['kecamatan']);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}
}
