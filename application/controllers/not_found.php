<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Not_found extends CI_Controller
{
    public function index()
    {
        // print_r($this->session->userdata('status'));
        $this->load->view('not_found');
    }
}