<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_skrd extends CI_Controller {

	public function index()
	{
		$this->load->model('laporan/cetak_skrd_m');
		$data = $this->cetak_skrd_m->config_datatable();
		$this->load->view('part/header');
		$this->load->view('laporan/cetak_skrd/datatable', $data);
		$this->load->view('part/footer');
	}


	public function datatable()
    {
        $this->load->model('laporan/cetak_skrd_m');
        $outp = $this->cetak_skrd_m->datatable($_POST);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($outp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

	}

	public function cetak()
	{
		$this->load->model('laporan/cetak_skrd_m');
        $outp = $this->cetak_skrd_m->cetak($_GET);
		$this->load->view('laporan/cetak_skrd/cetak_skrd', $outp);

	}

}
