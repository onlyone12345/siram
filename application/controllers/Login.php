<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct(){
		parent::__construct();		
        $this->load->model('login/Login_m');
		in_access_admin();
	}

    public function index()
    {
        // print_r($this->session->userdata('status'));
        $this->load->view('login'); 
    }

    public function login_action()
    {
        $username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => sha1($password)
			);
		$cek = $this->Login_m->cek_login("user",$where)->num_rows();
		if($cek > 0){
			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
            redirect(base_url('dashboard'));
            
		}else{
			$this->session->set_flashdata('flash_data', 'Username dan password salah !');
			$this->load->view('login');
			
		}
    }

}

?>