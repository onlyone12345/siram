<?php 
	function in_access_admin()
	{
		$ci=& get_instance();
		if($ci->session->userdata('status')=='login'){
			redirect('dashboard');
		}
	}

	function in_access_provider()
	{
		$ci=& get_instance();
		if($ci->session->userdata('status')=='login_provider'){
			redirect('dashboard');
		}
	}

	function no_access()
	{
		$ci=& get_instance();
		if(!$ci->session->userdata('status')){
			redirect('login');
		}
	}
	
?>