<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirm_m extends CI_Model
{
    public function get_select2_no_tagihan()
    {
        $this->db->select('id AS id, id AS text');
        $outp = $this->db->get('skrd');
        return $outp->result();
    }

    public function insert($data)
    {
        $return = FALSE;
		$insert =  $this->db->insert('konfirm', $data);
		if($insert){
			$return = TRUE;
		}
		return $return;
    }

    public function get_single($id_skrd)
    {
        $this->db->select('SUM(t.rpmt)-SUM(st.nominal_dibayarkan) AS harus_dibayar');
		$this->db->from('skrd_tower AS st');
		$this->db->join('tower AS t', 't.npwrd=st.npwrd', 'inner');
		$this->db->join('skrd AS s', 's.id=st.id_skrd', 'inner');
		$this->db->where('s.id', $id_skrd);

		$harus_dibayar = $this->db->get()->result_array();
	
		$return['harus_dibayar'] = $harus_dibayar[0]['harus_dibayar'];
		return $return;
    }

}