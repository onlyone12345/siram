<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skrd_provider_m extends CI_Model {
    
	public function config_datatable()
	{
		$data['title'] = 'Data SKRD';
		$data['datatable_url'] = 'provider/skrd/datatable';
		$data['add_url'] = 'provider/skrd/add';
		$data['delete_url'] = 'provider/skrd/delete';
		$data['datatable_header'] = '<tr>
										<th>NO Tagihan</th>
										<th>Tanggal Jatuh Tempo</th>
										<th>Keterangan</th>
										<th>Status</th>										
										<th width="120px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "no_tagihan"},',
			'{"data": "jatuh_tempo"},',
			'{"data": "keterangan"},',
			'{"data": "status"},',
			'{"data": "aksi"},',
		);

		return $data;
	}

	public function datatable($post)
    {    
        $total_data = $this->db->get('skrd')->num_rows();
		$total_filtered = $this
							->db
							->like('jatuh_tempo', $post['search']['value'])
							->or_like('keterangan', $post['search']['value'])
							->get('skrd')->num_rows();

        $this->db->select('*');
        $this->db->from('skrd');
		$this->db->like('jatuh_tempo', $post['search']['value']);
		$this->db->or_like('keterangan', $post['search']['value']);
        $this->db->order_by('id', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

		$column = array();
		
        foreach ($data->result() as $row) {
			$gg['no_tagihan'] = '#'.$row->id;
			$gg['jatuh_tempo'] = $row->jatuh_tempo;
			$gg['keterangan'] = $row->keterangan;
			$gg['status'] = '<a class="label label-danger">Belum Lunas</a>';
            $gg['aksi'] = '<div class="btn-group"><a href="'.site_url('provider/skrd/view/').$row->id.'" class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></div>';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
	}

	
	public function insert($data)
	{
		$insert =  $this->db->insert('skrd', $data);
		$id_skrd = $this->db->insert_id();

		$data_tower = $this->db->get('tower');
		print_r($data_tower->result());

		foreach ($data_tower->result() as $row)
		{
				$data_skrd_tower = array (
					"id_skrd" => $id_skrd,
					"npwrd" => $row->npwrd
				);
				
				$insert_tower = $this->db->insert('skrd_tower', $data_skrd_tower);
		}

		return $insert_tower;
	}
	
	public function edit($id = 1)
	{
		$this->db->select('pb.*, t.alamat, p.nama AS provider, t.rpmt');
		$this->db->from('provider AS pb');
		$this->db->join('tower AS t', 't.npwrd=pb.npwrd', 'inner');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'left');
		$this->db->where('pb.id', $id);
		$data = $this->db->get();
		if($data->num_rows() > 0){
			return $data->result_array()[0];
		}
	}

	public function update($id = 1, $data)
	{
		$return = FALSE;
		$this->db->where('id', $id);
		if ($this->db->update('provider', $data)) {
			$return = TRUE;
		};
		return $return;
	}

	public function delete($id)
	{
		$return = FALSE;
		if ($this->db->delete('skrd', array('id' => $id))) {
			$return = TRUE;
		};
		return $return;
    }
    
    public function get_select2()
    {
        $this->db->select('id, nama_provider AS text');
        $outp = $this->db->get('provider');
        return $outp->result();
    }

    public function get_alamat( $id = NULL )
    {
        $this->db->select('*');
        $this->db->where('id', 2);
        $outp = $this->db->get('provider');
        return $outp->result();
	}
	
	public function get_select2_tower()
	{
		$outp = $this->db->query('SELECT t.npwrd AS id, t.npwrd AS text FROM `tower` AS `t` WHERE t.npwrd NOT IN (SELECT `npwrd` FROM `provider` WHERE YEAR(`tgl_provider`)=YEAR(CURDATE()))');
       
        return $outp->result();
	}


	public function config_datatable_tower($id)
	{
		$data['title'] = 'data SKRD Tower No. Tagihan #'.$id;
		$data['datatable_url'] = 'provider/skrd/datatable_tower/'.$id;
		$data['add_url'] = 'provider/skrd/print_skrd/'.$id;
		$data['delete_url'] = 'provider/skrd/delete';
		$data['datatable_header'] = '<tr>
										<th>NO</th>
										<th>NPWRD</th>
										<th>Provider</th>
										<th>Jatuh Tempo</th>
										<th>Tagihan</th>
										<th>Status provider</th>
										<th width="120px">Action</th>
									</tr>';

		$data['datatable_column'] = array(
			'{"data": "no"},',
			'{"data": "npwrd"},',
			'{"data": "provider"},',
			'{"data": "jatuh_tempo"},',
			'{"data": "nominal"},',
			'{"data": "status_pembayaran"},',
			'{"data": "aksi"},',
		);
		return $data;
	}

	public function datatable_tower($id, $post)
    {  
        $total_data = $this->db->get('skrd_tower')->num_rows();
		$total_filtered = $this
							->db
							->like('npwrd', $post['search']['value'])
							// ->or_like('npwrd', $post['search']['value'])
							->get('skrd_tower')->num_rows();

        $this->db->select('st.*, p.nama AS provider, s.jatuh_tempo, t.rpmt AS nominal');
		$this->db->from('skrd_tower AS st');
		$this->db->join('tower AS t', 't.npwrd=st.npwrd', 'inner');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('skrd AS s', 's.id=st.id_skrd', 'inner');
		$this->db->where('s.id', $id);
		$this->db->where('p.id', $this->session->userdata('id_provider'));
		// $this->db->like('st.npwrd', $post['search']['value']);
		// $this->db->or_like('st.id', $post['search']['value']);
        $this->db->order_by('st.id', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

		$column = array();
		$no=1;
        foreach ($data->result() as $row) {
			$gg['no'] = $no++;
			$gg['npwrd'] = $row->npwrd;
			$gg['provider'] = $row->provider;
			$gg['jatuh_tempo'] = $row->jatuh_tempo;
			$gg['nominal'] = number_format($row->nominal);
			$gg['status_pembayaran'] = $row->status_pembayaran==0 ? $row->nominal_dibayarkan <= 0 ? '<a class="label label-danger">Belum Dibayarkan</a>': '<a class="label label-warning">Telah dibayar Rp. '. number_format($row->nominal_dibayarkan) .'</a>' : '<a class="label label-success">Telah Dibayarkan</a>';
            $gg['aksi'] = '<div class="btn-group"><a href="'.site_url('provider/skrd/print_single/').$row->id.'" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a></div>';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
	}

	public function print_skrd($id)
	{
		$this->db->select('t.*,k.nama_kecamatan AS kecamatan, p.nama AS provider, p.alamat AS alamat_provider');
		$this->db->from('tower AS t');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('kecamatan AS k', 'k.id=t.id_kecamatan', 'inner');
		$this->db->join('skrd_tower AS st', 'st.npwrd=t.npwrd', 'inner');
		$this->db->where('st.id_skrd', $id);
		$data['data'] = $this->db->get()->result_array();
		
		return $data;
	}

	public function print_single($id)
	{
		$this->db->select('t.*,k.nama_kecamatan AS kecamatan, p.nama AS provider, p.alamat AS alamat_provider');
		$this->db->from('tower AS t');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('kecamatan AS k', 'k.id=t.id_kecamatan', 'inner');
		$this->db->join('skrd_tower AS st', 'st.npwrd=t.npwrd', 'inner');
		$this->db->where('st.id', $id);
		$data['data'] = $this->db->get()->result_array();
		
		return $data;
	}

	
}
