<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tower_m extends CI_Model {
    
	public function config_datatable()
	{
		$data['title'] = 'data tower';
		$data['datatable_url'] = 'master/tower/datatable';
		$data['add_url'] = 'master/tower/add';
		$data['delete_url'] = 'master/tower/delete';
		$data['datatable_header'] = '<tr>
										<th>NPWRD</th>
										<th>Alamat</th>
										<th>Kecamatan</th>
										<th>Provider</th>
										<th>Alamat Provider</th>
										<th>RPMT</th>
										<th width="110px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "npwrd"},',
			'{"data": "alamat"},',
			'{"data": "kecamatan"},',
			'{"data": "provider"},',
			'{"data": "alamat_provider"},',
			'{"data": "rpmt", "className" : "text-right"},',
			'{"data": "aksi"},',
		);

		return $data;
	}

	public function datatable($post)
    {   
        $total_data = $this->db->get('tower')->num_rows();
		$total_filtered = $this->db->get('tower')->num_rows();
		
        $this->db->select('t.npwrd,t.alamat,k.nama_kecamatan AS kecamatan, t.rpmt, p.nama AS provider, p.alamat AS alamat_provider');
		$this->db->from('tower AS t');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('kecamatan AS k', 'k.id=t.id_kecamatan', 'inner');
		if(isset($post['filter']['id_kecamatan']) && !empty($post['filter']['id_kecamatan'])){
			$this->db->where('id_kecamatan', $post['filter']['id_kecamatan']);
		}
		if(isset($post['filter']['id_provider']) && !empty($post['filter']['id_provider'])){
			$this->db->where('id_provider', $post['filter']['id_provider']);
		}
        $this->db->order_by('t.npwrd', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

        $column = array();
        foreach ($data->result() as $row) {
			$gg['npwrd'] = $row->npwrd;
			$gg['alamat'] = $row->alamat;
			$gg['kecamatan'] = $row->kecamatan;
			$gg['provider'] = $row->provider;
			$gg['alamat_provider'] = $row->alamat_provider;
			$gg['rpmt'] = number_format($row->rpmt);
            $gg['aksi'] = '<div class="btn-group"><a href="'.site_url('master/tower/view/').$row->npwrd.'" class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a><a href="'.site_url('master/tower/edit/').$row->npwrd.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a><a data-toggle="modal" data-target=".modal_delete" data-id="'.$row->npwrd.'" class="btn btn-danger btn-xs delete_data"><i class="fa fa-trash"></i></a></div>';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
	}

	
	public function insert($data)
	{
		$return = FALSE;
		$insert =  $this->db->insert('tower', $data);
		if($insert){
			$return = TRUE;
		}
		return $return;
	}

	public function get_single($id)
	{
		$this->db->select('t.*, p.nama AS provider, p.alamat AS alamat_provider, k.nama_kecamatan AS kecamatan');
		$this->db->from('tower AS t');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('kecamatan AS k', 'k.id=t.id_kecamatan', 'inner');
		$this->db->where('npwrd', $id);
		$data = $this->db->get();
		if($data->num_rows() > 0){
			return $data->result_array()[0];
		}
	}
	
	public function edit($id)
	{
		return $this->get_single($id);
	}

	public function view($id)
	{
		return $this->get_single($id);
	}

	public function update($id = 1, $data)
	{
		$return = FALSE;
		$this->db->where('npwrd', $id);
		if ($this->db->update('tower', $data)) {
			$return = TRUE;
		};
		return $return;
	}

	public function delete($npwrd)
	{
		$return = FALSE;
		if ($this->db->delete('tower', array('npwrd' => $npwrd))) {
			$return = TRUE;
		};
		return $return;
	}

	public function get_npwrd($kecamatan, $id_provider, $npwrd_before)
	{	
		$data = $this->db->get('tower');
		$row = $data->num_rows();

		$data_provider = $this->db->select('alias')->where('id', $id_provider)->get('provider');
		$alias = $data_provider->result_array();
		$return = str_pad($row+1, 3, "0", STR_PAD_LEFT).'.'.$alias[0]['alias'].'.2019';
		return $return;
		
		// if(!empty($npwrd_before)){
		// 	$pad = str_pad($kecamatan, 2, "0", STR_PAD_LEFT).'.'.str_pad($id_provider, 3, "0", STR_PAD_LEFT).'.'.substr($npwrd_before,-4,4);
		// 	$return = $pad;
		// }else {
		// 	$data = $this->db->get('tower');
		// 	$row = $data->num_rows();
		// 	$pad = str_pad($kecamatan, 2, "0", STR_PAD_LEFT).'.'.str_pad($id_provider, 3, "0", STR_PAD_LEFT).'.'.str_pad($row+1, 4, "0", STR_PAD_LEFT);
		// 	$return = $pad;
		// }	
		// return $return;
	}

	public function get_select2()
	{
		$this->db->select('npwrd AS id, npwrd AS text');
        $outp = $this->db->get('tower');
        return $outp->result();
	}
}
