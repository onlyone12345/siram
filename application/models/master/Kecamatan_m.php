<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_m extends CI_Model {
    
	public function config_datatable()
	{
		$data['title'] = 'data kecamatan';
		$data['datatable_url'] = 'master/kecamatan/datatable';
		$data['add_url'] = 'master/kecamatan/add';
		$data['delete_url'] = 'master/kecamatan/delete';
		$data['datatable_header'] = '<tr>
										<th>Nama kecamatan</th>
										<th>Koefisien Jarak</th>
										<th width="120px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "nama_kecamatan"},',
			'{"data": "koefisien"},',
			'{"data": "aksi"},',
		);

		return $data;
	}

	public function datatable($post)
    {    
        $total_data = $this->db->get('kecamatan')->num_rows();
		$total_filtered = $this
							->db
							->like('nama_kecamatan', $post['search']['value'])
							->or_like('koefisien', $post['search']['value'])
							->get('kecamatan')->num_rows();

        $this->db->select('*');
        $this->db->from('kecamatan');
		$this->db->like('nama_kecamatan', $post['search']['value']);
		$this->db->or_like('koefisien', $post['search']['value']);
        $this->db->order_by('nama_kecamatan', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

        $column = array();
        foreach ($data->result() as $row) {
			$gg['nama_kecamatan'] = $row->nama_kecamatan;
			$gg['koefisien'] = $row->koefisien;
            $gg['aksi'] = '<div class="btn-group"><a href="'.site_url('master/kecamatan/edit/').$row->id.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a><a data-toggle="modal" data-target=".modal_delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs delete_data"><i class="fa fa-trash"></i></a></div>';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
	}

	
	public function insert($data)
	{
		$insert =  $this->db->insert('kecamatan', $data);
		return $insert;
	}
	
	public function edit($id = 1)
	{
		$data = $this->db->get_where('kecamatan',"id='".$id."'", 1);
		if($data->num_rows() > 0){
			return $data->result_array()[0];
		}
	}

	public function update($id = 1, $data)
	{
		$return = FALSE;
		$this->db->where('id', $id);
		if ($this->db->update('kecamatan', $data)) {
			$return = TRUE;
		};
		return $return;
	}

	public function delete($id)
	{
		$return = FALSE;
		if ($this->db->delete('kecamatan', array('id' => $id))) {
			$return = TRUE;
		};
		return $return;
    }
    
    public function get_select2()
    {
        $this->db->select('id, nama_kecamatan AS text');
        $outp = $this->db->get('kecamatan');
        return $outp->result();
    }

    public function get_alamat( $id = NULL )
    {
        $this->db->select('*');
        $this->db->where('id', 2);
        $outp = $this->db->get('kecamatan');
        return $outp->result();
	}
	
	public function get_single( $id = NULL )
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $outp = $this->db->get('kecamatan');
        return $outp->result();
    }
}
