<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provider_m extends CI_Model {
    
	public function config_datatable()
	{
		$data['title'] = 'data provider';
		$data['datatable_url'] = 'master/provider/datatable';
		$data['add_url'] = 'master/provider/add';
		$data['delete_url'] = 'master/provider/delete';
		$data['datatable_header'] = '<tr>
										<th>Nama Providder</th>
										<th>Alamat Provider</th>
										<th width="120px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "nama"},',
			'{"data": "alamat"},',
			'{"data": "aksi"},',
		);

		return $data;
	}

	public function datatable($post)
    {    
        $total_data = $this->db->get('provider')->num_rows();
		$total_filtered = $this
							->db
							->like('nama', $post['search']['value'])
							->or_like('alamat')
							->get('provider')->num_rows();

        $this->db->select('*');
        $this->db->from('provider');
		$this->db->like('nama', $post['search']['value']);
		$this->db->or_like('alamat', $post['search']['value']);
        $this->db->order_by('nama', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

        $column = array();
        foreach ($data->result() as $row) {
			$gg['nama'] = $row->nama;
			$gg['alamat'] = $row->alamat;
            $gg['aksi'] = '<div class="btn-group"><a href="'.site_url('master/provider/edit/').$row->id.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a><a data-toggle="modal" data-target=".modal_delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs delete_data"><i class="fa fa-trash"></i></a></div>';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
	}

	
	public function insert($data)
	{
		$insert =  $this->db->insert('provider', $data);
		return $insert;
	}
	
	public function edit($id = 1)
	{
		$data = $this->db->get_where('provider',"id='".$id."'", 1);
		if($data->num_rows() > 0){
			return $data->result_array()[0];
		}
	}

	public function update($id = 1, $data)
	{
		$return = FALSE;
		$this->db->where('id', $id);
		if ($this->db->update('provider', $data)) {
			$return = TRUE;
		};
		return $return;
	}

	public function delete($id)
	{
		$return = FALSE;
		if ($this->db->delete('provider', array('id' => $id))) {
			$return = TRUE;
		};
		return $return;
    }
    
    public function get_select2()
    {
        $this->db->select('id, nama AS text');
        $outp = $this->db->get('provider');
        return $outp->result();
    }

    public function get_alamat( $id = NULL )
    {
        $this->db->select('*');
        $this->db->where('id', 2);
        $outp = $this->db->get('provider');
        return $outp->result();
    }
}
