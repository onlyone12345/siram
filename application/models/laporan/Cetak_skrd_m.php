<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_skrd_m extends CI_Model {
    
	public function config_datatable()
	{
		$data['title'] = 'cetak SKRD';
		$data['datatable_url'] = 'laporan/cetak_skrd/datatable';
		$data['datatable_header'] = '<tr>
										<th>NPWRD</th>
										<th>Alamat</th>
										<th>Kecamatan</th>
										<th>Provider</th>
										<th>Alamat Provider</th>
										<th>RPMT</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "npwrd"},',
			'{"data": "alamat"},',
			'{"data": "kecamatan"},',
			'{"data": "provider"},',
			'{"data": "alamat_provider"},',
			'{"data": "rpmt", "className" : "text-right"},'
		);

		return $data;
	}

	public function datatable($post)
    {   
        $total_data = $this->db->get('tower')->num_rows();
		$total_filtered = $this->db->get('tower')->num_rows();
		
        $this->db->select('t.npwrd,t.alamat,k.nama_kecamatan AS kecamatan, t.rpmt, p.nama AS provider, p.alamat AS alamat_provider');
		$this->db->from('tower AS t');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('kecamatan AS k', 'k.id=t.id_kecamatan', 'inner');
		if(isset($post['filter']['id_kecamatan']) && !empty($post['filter']['id_kecamatan'])){
			$this->db->where('id_kecamatan', $post['filter']['id_kecamatan']);
		}
		if(isset($post['filter']['id_provider']) && !empty($post['filter']['id_provider'])){
			$this->db->where('id_provider', $post['filter']['id_provider']);
		}
        $this->db->order_by('t.npwrd', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

        $column = array();
        foreach ($data->result() as $row) {
			$gg['npwrd'] = $row->npwrd;
			$gg['alamat'] = $row->alamat;
			$gg['kecamatan'] = $row->kecamatan;
			$gg['provider'] = $row->provider;
			$gg['alamat_provider'] = $row->alamat_provider;
			$gg['rpmt'] = number_format($row->rpmt);
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        

        return $outp;
	}

	public function cetak($post)
	{
		$this->db->select('t.*,k.nama_kecamatan AS kecamatan, p.nama AS provider, p.alamat AS alamat_provider');
		$this->db->from('tower AS t');
		$this->db->join('provider AS p', 'p.id=t.id_provider', 'inner');
		$this->db->join('kecamatan AS k', 'k.id=t.id_kecamatan', 'inner');
		if(isset($post['filter']['id_kecamatan']) && !empty($post['filter']['id_kecamatan'])){
			$this->db->where('id_kecamatan', $post['filter']['id_kecamatan']);
		}
		if(isset($post['filter']['id_provider']) && !empty($post['filter']['id_provider'])){
			$this->db->where('id_provider', $post['filter']['id_provider']);
		}
        $this->db->order_by('t.npwrd', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
		$data['data'] = $this->db->get()->result_array();
		
		return $data;
	}


}
