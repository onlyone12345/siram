<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi_pembayaran_m extends CI_Model {
    
	public function config_datatable()
	{
		$data['title'] = 'data konfirmasi pembayaran';
		$data['datatable_url'] = 'pembayaran/konfirmasi_pembayaran/datatable';
		$data['add_url'] = 'pembayaran/konfirmasi_pembayaran/add';
		$data['delete_url'] = 'pembayaran/konfirmasi_pembayaran/delete';
		$data['datatable_header'] = '<tr>
										<th>No. SKRD</th>
										<th>Provider</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Status</th>
										<th width="120px">Action</th>
									</tr>';
		$data['datatable_column'] = array(
			'{"data": "id_skrd"},',
			'{"data": "provider"},',
			'{"data": "tanggal"},',
			'{"data": "keterangan"},',
			'{"data": "status"},',
			'{"data": "aksi"},',
		);

		return $data;
	}

	public function datatable($post)
    {    
        $total_data = $this->db->get('konfirm')->num_rows();
		$total_filtered = $this
							->db
							->like('id_skrd', $post['search']['value'])
							->get('konfirm')->num_rows();

        $this->db->select('k.*, p.nama AS provider, k.dibuat AS tanggal');
		$this->db->from('konfirm AS k');
		$this->db->join('provider AS p', 'p.id=k.id_provider', 'inner');		
		$this->db->like('p.nama', $post['search']['value']);
		// $this->db->or_like('tgl_pembayaran', $post['search']['value']);
        $this->db->order_by('id', $post['order'][0]['dir']);
        $this->db->limit($post['length'], $post['start']);
        $data = $this->db->get();

        $column = array();
        foreach ($data->result() as $row) {
			$gg['id_skrd'] = $row->id_skrd;
			$gg['provider'] = $row->provider;
			$gg['tanggal'] = $row->tanggal;
			$gg['keterangan'] = $row->keterangan;
			$gg['status'] = $row->konfirmasi==0 ? '<a class="label label-danger">Belum Dikonfirmasi</a>': '<a class="label label-success">Telah Dikonfirmasi</a>';
            $gg['aksi'] = $row->konfirmasi==0 ? '<div class="btn-group"><a href="'.site_url('pembayaran/konfirmasi_pembayaran/konfirm/').$row->id.'" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a></div>' : '-';
            $column[] = $gg;
        }

        $outp = array(
            'draw' => $post['draw'],
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_filtered,
            "data" => $column,
        );        
        return $outp;
	}

	public function insert($data)
	{
		$insert =  $this->db->insert('pembayaran', $data);
		return $insert;
	}
	
	public function edit($id)
	{
		$this->db->select('k.*, p.nama AS provider, k.dibuat AS tanggal');
		$this->db->from('konfirm AS k');
		$this->db->join('provider AS p', 'p.id=k.id_provider', 'inner');
		$this->db->where('k.id', $id);
		$data = $this->db->get();
		$return;
		if($data->num_rows() > 0){
			$return= $data->result_array()[0];
		}

		$this->db->select('SUM(t.rpmt)-SUM(st.nominal_dibayarkan) AS harus_dibayar');
		$this->db->from('skrd_tower AS st');
		$this->db->join('tower AS t', 't.npwrd=st.npwrd', 'inner');
		$this->db->join('skrd AS s', 's.id=st.id_skrd', 'inner');
		$this->db->join('konfirm AS k', 'k.id_skrd=s.id', 'inner');
		$this->db->where('k.id', $id);

		$harus_dibayar = $this->db->get()->result_array();
	
		$return['harus_dibayar'] = $harus_dibayar[0]['harus_dibayar'];
		return $return;
	}

	public function update($id = 1, $data)
	{
		$return = FALSE;
		$this->db->where('id', $id);
		if ($this->db->update('pembayaran', $data)) {
			$return = TRUE;
		};
		return $return;
	}

	public function delete($id)
	{
		$return = FALSE;
		if ($this->db->delete('pembayaran', array('id' => $id))) {
			$return = TRUE;
		};
		return $return;
    }
    
    public function get_select2()
    {
        $this->db->select('id, nama_pembayaran AS text');
        $outp = $this->db->get('pembayaran');
        return $outp->result();
    }

    public function get_alamat( $id = NULL )
    {
        $this->db->select('*');
        $this->db->where('id', 2);
        $outp = $this->db->get('pembayaran');
        return $outp->result();
	}
	
	public function get_select2_tower()
	{
		$outp = $this->db->query('SELECT t.npwrd AS id, t.npwrd AS text FROM `tower` AS `t` WHERE t.npwrd NOT IN (SELECT `npwrd` FROM `pembayaran` WHERE YEAR(`tgl_pembayaran`)=YEAR(CURDATE()))');
       
        return $outp->result();
	}

	public function konfirm_action($id)
	{
		$return = FALSE;
		$data = array(
			"konfirmasi" => 1
		);

		//masih error
		$this->db->select('st.*, t.rpmt, k.nominal');
		$this->db->from('skrd_tower AS st');
		$this->db->join('tower AS t', 't.npwrd=st.npwrd', 'inner');
		$this->db->join('skrd AS s', 's.id=st.id_skrd', 'inner');
		$this->db->join('konfirm AS k', 'k.id_skrd=s.id', 'inner');
		$this->db->where('k.id', $id);
		$this->db->where('st.status_pembayaran', 0);
		$data_skrd = $this->db->get()->result();
		
		$nominal_bayar = $data_skrd[0]->nominal;
		$bayar = array();
		foreach ($data_skrd as $row) {
			$nominal_bayar = $nominal_bayar + $row->nominal_dibayarkan;

			if($nominal_bayar >= $row->rpmt){	
				$this->db->where('npwrd', $row->npwrd);
				$this->db->where('id_skrd', $row->id_skrd);
				$this->db->update('skrd_tower', array("nominal_dibayarkan"=> $row->rpmt, "status_pembayaran" => 1 ));
				$return = TRUE;
			}else if($nominal_bayar < $row->rpmt && $nominal_bayar >= 0){
				$this->db->where('npwrd', $row->npwrd);
				$this->db->where('id_skrd', $row->id_skrd);
				$this->db->update('skrd_tower', array("nominal_dibayarkan"=>  $nominal_bayar + $row->nominal_dibayarkan, "status_pembayaran" => 0 ));
				$return = TRUE;
			}else {
				$this->db->where('npwrd', $row->npwrd);
				$this->db->where('id_skrd', $row->id_skrd);
				$this->db->update('skrd_tower', array("nominal_dibayarkan"=> 0, "status_pembayaran" => 0 ));
				$return = TRUE;
			}
			$nominal_bayar= $nominal_bayar - $row->rpmt;
		}
		return $return;
	}

}
